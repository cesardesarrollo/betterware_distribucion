
/***************** Vistazo de distribución para Betterware *****************/
/***************** Desarrollado por César Alonso Magaña G. *****************/

/***************** CONSTRUCTOR *****************/


function Nivel(){
    this.nivel = {};
    this.base_url = "";
    this.api_paginado = "http://betterapp.betterware.com.mx:8000/ws/distribution_view/?clave=";

}


/***************** AJAX REQUESTs *****************/

Nivel.prototype._getDistribution = function(){
    var self = this;
	$('#modal-wait').modal('show');

	$.ajax({
		url 		: self.api_paginado + self.nivel.id,
		method 		: 'GET',
		dataType	: 'JSON',
		data 		: self.nivel
	})
	.done( function( _res ){
		$('#modal-wait').modal('hide');
		self._drawDistribution( _res );
	})
	.fail( function( _err ){
		$('#modal-wait').modal('hide');
		alert("No existen datos para esta persona1");
		console.log( _err );
	})
}

/***************** Seteo  *****************/


Nivel.prototype._set = function( _data ){
    this.nivel.key    = _data._key || null;
    this.nivel.id     = _data._id || null;
    this.nivel.level  = _data._level || null;
    this.nivel.page   = _data._page || null;
    this.nivel.method = _data._method || null;

    if (this.nivel.method === "getDistribution"){ 
        this._getDistribution();
    }

}

/***************** Funciones comúnes **************/


// Formatea tipo moneda
Number.prototype._formatMoney = function(c, d, t){
var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return "$ " + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };

// Regresa datos formateados
Nivel.prototype._formatData = function( _data ){

	// COMPRAS
	for (var i = 0; i <= _data.compras.shoppings_view_detail.length - 1; i++) {
		// Format Money
		var cantidad = +_data.compras.shoppings_view_detail[i].monto;
		_data.compras.shoppings_view_detail[i].monto = (cantidad)._formatMoney(2, '.', ',');
	}

	for (var i = 0; i <= _data.compras.shoppings_view.length - 1; i++) {
		// Format Money
		var cantidad = +_data.compras.shoppings_view[i].importeCatalogoActual;
		_data.compras.shoppings_view[i].importeCatalogoActual = (cantidad)._formatMoney(2, '.', ',');
		// Format Money
		var cantidad = +_data.compras.shoppings_view[i].importeCatalogoAnterior;
		_data.compras.shoppings_view[i].importeCatalogoAnterior = (cantidad)._formatMoney(2, '.', ',');
		// Format Money
		var cantidad = +_data.compras.shoppings_view[i].ventaParaAlcanzarNivel;
		_data.compras.shoppings_view[i].ventaParaAlcanzarNivel = (cantidad)._formatMoney(2, '.', ',');
	}

	// CRÉDITO
	for (var i = 0; i <= _data.credito.balance_charger_validity.length - 1; i++) {
		// Format Money
		var cantidad = +_data.credito.balance_charger_validity[i].H7Dias;
		_data.credito.balance_charger_validity[i].H7Dias = (cantidad)._formatMoney(2, '.', ',');
		// Format Money
		var cantidad = +_data.credito.balance_charger_validity[i].mas7Dias;
		_data.credito.balance_charger_validity[i].mas7Dias = (cantidad)._formatMoney(2, '.', ',');
		// Format Money
		var cantidad = +_data.credito.balance_charger_validity[i].mas14Dias;
		_data.credito.balance_charger_validity[i].mas14Dias = (cantidad)._formatMoney(2, '.', ',');
		// Format Money
		var cantidad = +_data.credito.balance_charger_validity[i].mas21Dias;
		_data.credito.balance_charger_validity[i].mas21Dias = (cantidad)._formatMoney(2, '.', ',');
	}

	for (var i = 0; i <= _data.credito.last_store.length - 1; i++) {
		// Format Money
		var cantidad = +_data.credito.last_store[i].Importe;
		_data.credito.last_store[i].Importe = (cantidad)._formatMoney(2, '.', ',');
	}

	for (var i = 0; i <= _data.credito.last_returns.length - 1; i++) {
		// Format Money
		var cantidad = +_data.credito.last_returns[i].Monto;
		_data.credito.last_returns[i].Monto = (cantidad)._formatMoney(2, '.', ',');

		if (_data.credito.last_returns[i].Estatus === "Aceptado"){
			_data.credito.last_returns[i].estatus_icon = "aceptado";
		} else {
			_data.credito.last_returns[i].estatus_icon = "rechazado";
		}

	}

	// ENTREGAS
	for (var i = 0; i <= _data.entregas.order_in_transit.length - 1; i++) {
		// Format Money
		var cantidad = +_data.entregas.order_in_transit[i].Monto;
		_data.entregas.order_in_transit[i].Monto = (cantidad)._formatMoney(2, '.', ',');
	}

	return _data;
}


/***************** Handlebars  *****************/


// Cabecera
Nivel.prototype._drawDistribution = function( _data ){

	var self = this;
	if ( _data !== undefined ){

		$.ajax({
			url: "templates/mainDistribution.hbs",
			dataType: "html",
			success: function (_template) {

				// Formateo de datos	
				_data = self._formatData(_data);
				console.log("_data",_data);

				// COMPILAR TEMPLATE
				var template = Handlebars.compile( _template );
				var html = template( _data );
				$("#distribution").addClass('animated fadeIn').html( html );

			}
		});

	} else {
		alert("No existen datos para esta persona");
	}
};


/***************** Acciones  *****************/


// Modal con Información de Superiores
$('#mdlsuperior').on('show.bs.modal', function (event) {

	var button = $(event.relatedTarget)
	var clave = button[0].dataset.clave

	var modal = $(this)

	// Limpieza de datos anteriores
	modal.find('.modal-title').text("Información")
	modal.find('#celular').text("")
	modal.find('#telefono').text("")
	modal.find('#direccion').text("")
	modal.find('#mail').text("")
	modal.find('#nombre').text("")

	var params = {
		'base_url': 'http://ec2-34-194-194-71.compute-1.amazonaws.com:8000/ws/detail_person/?clave=' + clave
	}
	$.ajax({
		url     : params.base_url,
		method  : 'GET',
		async   : true,
		data    : params
	})
	.done(function( _res ){
		if(_res.data.Nombre !== null){
			modal.find('.modal-title').text("Información de " + _res.data.Nombre)
			modal.find('#celular').text(_res.data.Celular)
			modal.find('#telefono').text(_res.data.Telefono)
			modal.find('#direccion').text(_res.data.Direccion)
			modal.find('#mail').text(_res.data.Mail)
			modal.find('#nombre').text(_res.data.Nombre)
		} else {
			modal.modal("hide");
			alert("No hay detalles disponible de esta persona");
		}
	})
	.fail(function( _err ){
		console.log( _err );
	})

});